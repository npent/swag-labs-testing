*** Settings ***
Resource            resource.robot

Suite Setup         Prepare Browser
Suite Teardown      Close Browser 

Test Setup          Open Obstacle
Test Teardown       Verify Solved SORT PRICE HIGH TO LOW

*** Test Cases ***

SORT
    Wait Until Page Contains Element    //input[@id='login-button']
    Input Text                          id=user-name    standard_user
    Input Text                          id=password     secret_sauce
    Click Element                       //input[@id='login-button']
    Wait Until Page Contains Element    //select[@class='product_sort_container']
    Click Element                       //select[@class='product_sort_container']
    Click Element                       //option[@value='za']
    Verify Solved SORT Z TO A
    Click Element                       //select[@class='product_sort_container']
    Click Element                       //option[@value='az']
    Verify Solved SORT A TO Z
    Click Element                       //select[@class='product_sort_container']
    Click Element                       //option[@value='lohi']
    Verify Solved SORT PRICE LOW TO HIGH
    Click Element                       //select[@class='product_sort_container']
    Click Element                       //option[@value='hilo']
    Verify Solved SORT PRICE HIGH TO LOW

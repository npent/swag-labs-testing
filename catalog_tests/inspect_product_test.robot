*** Settings ***
Resource            resource.robot

Suite Setup         Prepare Browser
Suite Teardown      Close Browser 

Test Setup          Open Obstacle
Test Teardown       Verify Solved RETURN TO CATALOG

*** Test Cases ***

INSPECT PRODUCT
    Wait Until Page Contains Element    //input[@id='login-button']
    Input Text                          id=user-name    standard_user
    Input Text                          id=password     secret_sauce
    Click Element                       //input[@id='login-button']
    Click Element                       id=item_4_title_link
    Verify Solved INSPECT PRODUCT
    Click Element                       id=back-to-products
    Verify Solved RETURN TO CATALOG

*** Settings ***
Library           SeleniumLibrary   run_on_failure=Nothing

*** Variables ***
${SERVER}         https://www.saucedemo.com/
${BROWSER}        Chrome
${DRIVER}         rf-env/WebDriverManager/chrome/90.0.4430.24/chromedriver_win32/chromedriver.exe
${DELAY}          0.2

*** Keywords ***
Prepare Browser
    Open Browser    ${SERVER}List    ${BROWSER}   executable_path=${DRIVER}
    Maximize Browser Window
    Set Selenium Speed    ${DELAY}

Open Obstacle
    Go To   ${SERVER}

Verify Solved INSPECT PRODUCT
    Wait Until Page Contains    Back to products
    
Verify Solved RETURN TO CATALOG
    Wait Until Page Contains    Products

Verify Solved SORT A TO Z
    Element Should Contain      //span[@class='active_option']      NAME (A TO Z)

Verify Solved SORT Z TO A
    Element Should Contain      //span[@class='active_option']      NAME (Z TO A)

Verify Solved SORT PRICE LOW TO HIGH
    Element Should Contain      //span[@class='active_option']      PRICE (LOW TO HIGH)

Verify Solved SORT PRICE HIGH TO LOW
    Element Should Contain      //span[@class='active_option']      PRICE (HIGH TO LOW)

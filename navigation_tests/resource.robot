*** Settings ***
Library           SeleniumLibrary   run_on_failure=Nothing

*** Variables ***
${SERVER}         https://www.saucedemo.com/
${BROWSER}        Chrome
${DRIVER}         rf-env/WebDriverManager/chrome/90.0.4430.24/chromedriver_win32/chromedriver.exe
${DELAY}          0.2

*** Keywords ***
Prepare Browser
    Open Browser    ${SERVER}List    ${BROWSER}   executable_path=${DRIVER}
    Maximize Browser Window
    Set Selenium Speed    ${DELAY}

Open Obstacle
    Go To   ${SERVER}

Verify Solved ALL ITEMS
    Wait Until Location Is    https://www.saucedemo.com/inventory.html

Verify Solved ABOUT
    Wait Until Location Is    https://saucelabs.com/

Verify Solved LOGOUT
    Wait Until Location Is    https://www.saucedemo.com/

Verify Solved RESET APP STATE
    Page Should Not Contain Element      css:span.shopping_cart_badge

Verify Solved TWITTER
    Page Should Contain Link        https://twitter.com/saucelabs

Verify Solved FACEBOOK
    Page Should Contain Link        https://www.facebook.com/saucelabs

Verify Solved LINKEDIN
    Page Should Contain Link        https://www.linkedin.com/company/sauce-labs/

LOGIN
    Wait Until Page Contains Element    //input[@id='login-button']
    Input Text                          id=user-name    standard_user
    Input Text                          id=password     secret_sauce
    Click Element                       //input[@id='login-button']
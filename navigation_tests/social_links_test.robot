*** Settings ***
Resource            resource.robot

Suite Setup         Prepare Browser
Suite Teardown      Close Browser 

Test Setup          Open Obstacle      

*** Test Cases ***
SOCIAL ICON LINKS
    LOGIN
    Wait Until Page Contains Element    //li[@class='social_twitter']
    Verify Solved TWITTER
    Verify Solved FACEBOOK
    Verify Solved LINKEDIN
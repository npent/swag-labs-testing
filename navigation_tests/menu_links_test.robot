*** Settings ***
Resource            resource.robot

Suite Setup         Prepare Browser
Suite Teardown      Close Browser 

Test Setup          Open Obstacle
Test Teardown       

*** Test Cases ***
CLICK ALL ITEMS
    LOGIN
    Wait Until Page Contains Element    //button[@id='react-burger-menu-btn']
    Click Element                       //button[@id='react-burger-menu-btn']
    Wait Until Page Contains Element    //a[@id='inventory_sidebar_link']
    Click Element                       //a[@id='inventory_sidebar_link']
    Verify Solved ALL ITEMS

CLICK ABOUT
    LOGIN
    Wait Until Page Contains Element    //button[@id='react-burger-menu-btn']
    Click Element                       //button[@id='react-burger-menu-btn']
    Wait Until Page Contains Element    //a[@id='about_sidebar_link']
    Click Element                       //a[@id='about_sidebar_link']
    Verify Solved ABOUT

CLICK LOGOUT
    LOGIN
    Wait Until Page Contains Element    //button[@id='react-burger-menu-btn']
    Click Element                       //button[@id='react-burger-menu-btn']
    Wait Until Page Contains Element    //a[@id='logout_sidebar_link']
    Click Element                       //a[@id='logout_sidebar_link']
    Verify Solved LOGOUT

CLICK RESET
    LOGIN
    Click Element                       //button[@id='add-to-cart-sauce-labs-fleece-jacket']
    Wait Until Page Contains Element    //button[@id='react-burger-menu-btn']
    Click Element                       //button[@id='react-burger-menu-btn']
    Wait Until Page Contains Element    //a[@id='reset_sidebar_link']
    Click Element                       //a[@id='reset_sidebar_link']
    Verify Solved RESET APP STATE
*** Settings ***
Resource            resource.robot

Suite Setup         Prepare Browser
Suite Teardown      Close Browser 

Test Setup          Open Obstacle

*** Test Cases ***

CHECKOUT
    Wait Until Page Contains Element    //input[@id='login-button']
    Input Text                          id=user-name    standard_user
    Input Text                          id=password     secret_sauce
    Click Element                       //input[@id='login-button']
    Click Element                       id=add-to-cart-sauce-labs-backpack
    Click Element                       id=shopping_cart_container
    Click Element                       id=checkout
    Input Text                          id=first-name    aku
    Input Text                          id=last-name     ankka
    Input Text                          id=postal-code   33100
    Click Element                       id=continue
    Verify Solved CHECKOUT OVERVIEW

FINISH ORDER
    Wait Until Page Contains Element    //input[@id='login-button']
    Input Text                          id=user-name    standard_user
    Input Text                          id=password     secret_sauce
    Click Element                       //input[@id='login-button']
    Click Element                       id=shopping_cart_container
    Click Element                       id=checkout
    Input Text                          id=first-name    aku
    Input Text                          id=last-name     ankka
    Input Text                          id=postal-code   33100
    Click Element                       id=continue
    Verify Solved CHECKOUT OVERVIEW
    Click Element                       id=finish
    Verify Solved ORDER FINISHED

CANCEL ORDER
    Wait Until Page Contains Element    //input[@id='login-button']
    Input Text                          id=user-name    standard_user
    Input Text                          id=password     secret_sauce
    Click Element                       //input[@id='login-button']
    Click Element                       id=shopping_cart_container
    Click Element                       id=checkout
    Input Text                          id=first-name    aku
    Input Text                          id=last-name     ankka
    Input Text                          id=postal-code   33100
    Click Element                       id=continue
    Verify Solved CHECKOUT OVERVIEW
    Click Element                       id=cancel
    Verify Solved ORDER CANCELLED

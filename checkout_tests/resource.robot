*** Settings ***
Library           SeleniumLibrary   run_on_failure=Nothing

*** Variables ***
${SERVER}         https://www.saucedemo.com/
${BROWSER}        Chrome
${DRIVER}         rf-env/WebDriverManager/chrome/90.0.4430.24/chromedriver_win32/chromedriver.exe
${DELAY}          0.2

*** Keywords ***
Prepare Browser
    Open Browser    ${SERVER}List    ${BROWSER}   executable_path=${DRIVER}
    Maximize Browser Window
    Set Selenium Speed    ${DELAY}

Open Obstacle
    Go To   ${SERVER}
    
Verify Solved CHECKOUT OVERVIEW
    Wait Until Page Contains    Checkout: Overview

Verify Solved ORDER FINISHED
    Wait Until Page Contains    Your order has been dispatched, and will arrive just as fast as the pony can get there!

Verify Solved ORDER CANCELLED
    Wait Until Page Contains    Products
    


